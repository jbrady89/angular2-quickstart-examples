import { 
    Component, OnInit, trigger,
    state,
    style,
    transition,
    animate 
} from '@angular/core';
import { Router } from '@angular/router';
import { Hero } from './hero';
import { HeroService } from './hero.service';

@Component({
  moduleId: module.id,
  selector: 'my-heroes',
  templateUrl: 'heroes.component.html',
  styleUrls: [ 'heroes.component.css' ],
  animations: [
    trigger('heroState', [
        state('inactive', style({transform: 'translateX(0) scale(1)'})),
        state('active',   style({transform: 'translateX(0) scale(1.1)'})),
        transition('inactive => active', animate('100ms ease-in')),
        transition('active => inactive', animate('100ms ease-out')),
        transition('void => inactive', [
          style({transform: 'translateX(-100%) scale(1)'}),
          animate(100)
        ]),
        //transition('inactive => void', [
        //  animate(100, style({transform: 'translateX(100%) scale(1)'}))
        //]),
        //transition('void => active', [
        //  style({transform: 'translateX(0)'}),
        //  animate(200)
        //]),
        //transition('active => void', [
          //animate(200, style({transform: 'translateX(0) scale(0)'}))
        //])
    ]),
    trigger('shrinkOut', [
        state('in', style({height: '*'})),
        transition('* => void', [
          style({height: '*'}),
          animate(250, style({height: 0}))
        ])
      ])
  ]
})

export class HeroesComponent implements OnInit  { 

    heroes: Hero[];
    selectedHero: Hero;
    
    constructor(private heroService: HeroService, private router: Router) { }
    
    ngOnInit(): void {
        this.getHeroes();
    }
    
    getHeroes(): void {
    
        //this.heroes = this.heroService.getHeroes();
        
        // use fat arrow so that this refers to AppComponent
        this.heroService.getHeroesSlowly().then(heroes => this.heroes = heroes);

    }
    
    gotoDetail(): void {
        console.log("go to detail view");
        this.router.navigate(['/detail', this.selectedHero.id]);

    }
    
    onSelect(hero: Hero): void {
        this.selectedHero = hero;
    }
    
    delete(hero: Hero): void {
        this.heroService
          .delete(hero.id)
          .then(() => {
            this.heroes = this.heroes.filter(h => h !== hero);
            if (this.selectedHero === hero) { this.selectedHero = null; }
          });
    }
    
    add(name: string): void {
      name = name.trim();
      if (!name) { return; }
      this.heroService.create(name)
        .then(hero =>   {
          this.heroes.push(hero);
          this.selectedHero = null;
        });
    }
}
